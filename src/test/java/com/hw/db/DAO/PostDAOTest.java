package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.stream.Stream;

import static com.hw.db.DAO.PostDAO.setPost;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class PostDAOTest {

    private static final JdbcTemplate jdbc = mock(JdbcTemplate.class);

    @BeforeAll
    static void setUp() {
        new PostDAO(jdbc);
        when(jdbc.queryForObject(any(), any(PostDAO.PostMapper.class), any()))
                .thenReturn(new Post("author2", new Timestamp(2), "forum2", "message2", null, null, true));
    }

    @ParameterizedTest
    @MethodSource("setPostArguments")
    void testMethodSetPost(Post post, String sql) {
        setPost(111111, post);
        verify(jdbc).update(eq(sql), ArgumentMatchers.<Object>any());
    }

    private static Stream<Arguments> setPostArguments() {
        return Stream.of(
                Arguments.of(
                        new Post("author1", new Timestamp(1), "forum1", "message1", null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post(null, new Timestamp(1), "forum1", null, null, null, true),
                        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author1", null, "forum1", null, null, null, true),
                        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
                ),

                Arguments.of(
                        new Post(null, new Timestamp(1), "forum1", "message1", null, null, true),
                        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author1", null, "forum1", "message1", null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post("author1", new Timestamp(1), "forum1", null, null, null, true),
                        "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
                ),
                Arguments.of(
                        new Post(null, null, "forum1", "message1", null, null, true),
                        "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"
                )
        );
    }
}
