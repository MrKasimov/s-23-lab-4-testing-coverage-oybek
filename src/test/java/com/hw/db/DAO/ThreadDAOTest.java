package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static com.hw.db.DAO.ThreadDAO.treeSort;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDAOTest {

    private static final JdbcTemplate jdbc = mock(JdbcTemplate.class);

    @BeforeAll
    static void setUp() {
        new ThreadDAO(jdbc);
    }

    @ParameterizedTest
    @MethodSource("treeSortArguments")
    void testMethodTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String sql) {
        treeSort(id, limit, since, desc);
        verify(jdbc).query(eq(sql), any(PostDAO.PostMapper.class), any());
    }

    private static Stream<Arguments> treeSortArguments() {
        return Stream.of(
                Arguments.of(
                        11111, 1, 111, true,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"
                ),
                Arguments.of(
                        11111, 1, 111, false,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"
                ),
                Arguments.of(
                        11111, null, 111, false,
                        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"
                ),
                Arguments.of(
                        11111, null, null, null,
                        "SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"
                )
        );
    }
}
