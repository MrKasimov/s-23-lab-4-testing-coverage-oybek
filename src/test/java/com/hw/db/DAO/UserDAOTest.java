package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentMatchers;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static com.hw.db.DAO.UserDAO.Change;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class UserDAOTest {

    private static final JdbcTemplate jdbc = mock(JdbcTemplate.class);

    @BeforeAll
    static void setUp() {
        new UserDAO(jdbc);
    }

    @ParameterizedTest
    @MethodSource("ChangeArguments")
    void testMethodChange(User user, String sql) {
        Change(user);
        verify(jdbc).update(eq(sql), ArgumentMatchers.<Object>any());
    }

    private static Stream<Object> ChangeArguments() {
        return Stream.of(
                Arguments.of(
                        new User("nickname", "email.com", null, null),
                        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", null, null, "about"),
                        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", "email.com", null, "about"),
                        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", "email.com", "fullname", "about"),
                        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", null, "fullname", "about"),
                        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", "email.com", "fullname", null),
                        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"
                ),
                Arguments.of(
                        new User("nickname", null, "fullname", null),
                        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"
                )
        );
    }
}
