package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.stream.Stream;

import static com.hw.db.DAO.ForumDAO.UserList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTest {

    private static final JdbcTemplate jdbc = mock(JdbcTemplate.class);

    @BeforeAll
    static void setUp() {
        new ForumDAO(jdbc);
    }

    @ParameterizedTest
    @MethodSource("userListArguments")
    void testMethodUserList(Number limit, String since, Boolean desc, String sql) {
        UserList("slugTest", limit, since, desc);
        verify(jdbc).query(eq(sql), any(Object[].class), any(UserDAO.UserMapper.class));
    }

    private static Stream<Arguments> userListArguments() {
        return Stream.of(
                Arguments.of(null, null, false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Arguments.of(null, "12.12.2012", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Arguments.of(10, "12.12.2012", true, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Arguments.of(10, "12.12.2012", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Arguments.of(null, "12.12.2012", false, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;")
        );
    }
}
